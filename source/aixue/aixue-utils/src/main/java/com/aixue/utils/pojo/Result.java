package com.aixue.utils.pojo;

public class Result{

	private int code;
	
	private String message;
	
	private boolean result;
	
	private Object data;

	public Result() {
		code = 200;
		result = false;
	}
	
	
	public Result(int code, String message, boolean result, Object data) {
		this.code = code;
		this.message = message;
		this.result = result;
		this.data = data;
	}

    public static Result failed(int i, String s) {
		return new Result(i,s ,false,null);
    }


    public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public static Result success(String message,Object data){
		return new Result(200,message,true,data);
	}

	public static Result unservice(String message,Object data){
		return new Result(500,"Service not available",false,data);
	}
}
