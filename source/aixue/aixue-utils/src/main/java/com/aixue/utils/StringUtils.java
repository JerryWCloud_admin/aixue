package com.aixue.utils;

import java.util.UUID;

public class StringUtils {

	public StringUtils() {
	}

	public static boolean isEmpty(String... strs) {

		for (int i = 0; i < strs.length; i++) {
			if (null == strs[i] || "".equals(strs[i])) {
				return true;
			}
		}
		return false;
	}

	public static boolean notEmpty(String... strs) {
		return !isEmpty(strs);
	}
	
	public static String uuid() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
	public static Integer parseInteger(Object o) {
	    return matches(o,"^\\d+$") ? Integer.valueOf(o.toString())  : null;
	}
	
	public static Integer parseInteger(Object o,Integer def) {
        return matches(o,"^\\d+$") ? Integer.valueOf(o.toString()) : def;
    }
	
    /**
     * 字符串匹配
     *
     * @param value
     * @param regex
     * @return
     */
    public static boolean matches(Object value, String regex) {
        if (null == value) {
            return false;
        }
        return value.toString().matches(regex);
    }

	/**
	 * 空值的替换方法
	 * @param o
	 * @param t
	 * @param <T>
	 * @return
	 */
	public static <T> T notNull(Object o, T t){

		if(o == null)
			return t;
		else if (o instanceof Integer && t instanceof Integer)
			return (T) o;
		else if (o instanceof String && t instanceof String)
			return (T) o;
		else if (o instanceof Float && t instanceof Float)
			return (T) o;
		return t;
	}
}
