package com.aixue.utils;

import com.aixue.utils.pojo.Result;
import com.alibaba.fastjson.JSON;

public class BeanUtils {

    public static  <Bean> Bean resultToBean(Result r, Class<Bean> clazz){
        if(r.isResult() && r.getData() != null){

            return JSON.parseObject(r.getData().toString(),clazz);
        }
        return null;
    }
}
