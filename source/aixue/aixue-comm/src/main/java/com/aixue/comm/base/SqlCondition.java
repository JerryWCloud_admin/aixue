package com.aixue.comm.base;

import lombok.Data;

@Data
public class SqlCondition {

    private String name;

    private String value;

    private String method;

    interface  Conditions{
        String EQ = "eq";
        String GT = "gt";
        String LT = "lt";
    }
}
