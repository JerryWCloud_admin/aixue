package com.aixue.comm.base;

import java.util.Map;
import java.util.Optional;

import com.aixue.comm.pojo.QueryLIstParam;
import com.aixue.utils.StringUtils;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.aixue.comm.bean.BaseBean;
import com.aixue.utils.pojo.Result;
import org.springframework.web.bind.annotation.RequestMethod;

public abstract class BaseController<T extends BaseBean> {

    protected static final Logger log = LoggerFactory.getLogger(BaseController.class);

    protected abstract BaseService getBaseService();

    /**T
     * 通用的添加方法
     *
     * @return 用户列表
     */

    @RequestMapping(value = "/insert" , method = RequestMethod.POST)
    public Result insert(@RequestBody T t) {
        log.info("通用的添加数据方法,{}",t);
        if(t == null){
            return Result.failed(500,"request body can not null!");
        }
        //校验
        Optional<Result> validate = t.validate(ValidateType.INSERT);
        if(validate.isPresent()){
            return validate.get();
        }

        Integer result = getBaseService().insert(t);
        if(1 == result){
            return Result.success("",null);
        }

        return Result.failed(500,"insert failed!");
    }

    /**
     * 通用的查一条数据方法
     *
     * @return  一条数据
     */
    @RequestMapping(value = "/query/{id}" ,method = RequestMethod.GET)
    public Result query(@PathVariable String id) {
        log.info("通用的查一条数据方法,{}",id);
        if(id == null){
            return Result.failed(500,"request id can not null!");
        }
        return Result.success("成功",getBaseService().selectById(id));
    }

    /**
     * 通用的删除一条数据方法
     *
     * @return  删除一条数据
     */
    @RequestMapping(value = "/del/{id}" , method = RequestMethod.GET)
    public Result del(@PathVariable String id) {
        log.info("通用的查一条数据方法,{}",id);
        if(id == null){
            return Result.failed(500,"request id can not null!");
        }
        Integer result = getBaseService().deleteById(id);
        if(1 == result){
            return Result.success("成功",null);
        }
        return Result.failed(500,"删除失败!");
    }

    /**
     * 通用的修改一条数据方法
     *
     * @return  修改一条数据
     */

    @RequestMapping(value = "/update/{id}" , method = RequestMethod.GET)
    public Result update(@RequestBody T t) {
        log.info("通用的修改一条数据方法,{}",t);
        if(t == null){
            return Result.failed(500,"request update body can not null!");
        }
        //校验
        Optional<Result> validate = t.validate(ValidateType.UPDATE);
        if(validate.isPresent()){
            return validate.get();
        }

        Integer result = getBaseService().updateById(t);
        if(1 == result){
            return Result.success("成功",null);
        }
        return Result.failed(500,"失败!");
    }

    /**
     * 通用查询数据列表的方法，含有分页的功能
     *
     * @return  修改一条数据
     */

    @RequestMapping(value = "/queryList" , method = RequestMethod.POST)
    public Result queryList(@RequestBody QueryLIstParam param) {
        log.info("通用查询数据列表的方法,{}",param);

        if(param == null){
            return Result.failed(500,"request param body can not null!");
        }

        Page<T> pageEntity = new Page<>();
        pageEntity.setSize(StringUtils.notNull(param.getPageSize(),10));
        pageEntity.setCurrent(StringUtils.notNull(param.getPageNum(),1));

        QueryWrapper<T> wrapper = new QueryWrapper<>();
        param.getConditions().forEach( e -> {
            switch (e.getMethod()){
                case SqlCondition.Conditions.EQ:
                    wrapper.eq(e.getName(),e.getValue());
                case SqlCondition.Conditions.GT:
                    wrapper.gt(e.getName(),e.getValue());
                case SqlCondition.Conditions.LT:
                    wrapper.lt(e.getName(),e.getValue());
            }
        });

        return Result.success("成功",getBaseService().selectPage(pageEntity, wrapper));
    }
}
