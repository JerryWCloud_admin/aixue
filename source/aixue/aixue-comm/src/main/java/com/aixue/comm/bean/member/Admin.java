package com.aixue.comm.bean.member;

import com.aixue.comm.bean.BaseBean;

import lombok.Data;

@Data
public class Admin extends BaseBean{
	
    private String id;

    private String account;

    private String password;

    private String nickname;

    private String profile;

 
}