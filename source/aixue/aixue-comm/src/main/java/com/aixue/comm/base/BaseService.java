package com.aixue.comm.base;

import com.aixue.comm.bean.BaseBean;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface BaseService<T extends BaseBean> {

    Integer insert(T var1);

    Integer deleteById(Serializable var1);

    Integer deleteByMap(@Param("cm") Map<String, Object> var1);

    Integer delete(@Param("ew") Wrapper<T> var1);

    Integer deleteBatchIds(List<? extends Serializable> var1);

    Integer updateById(@Param("et") T var1);

    Integer update(@Param("et") T var1, @Param("ew") Wrapper<T> var2);

    T selectById(Serializable var1);

    List<T> selectBatchIds(List<? extends Serializable> var1);

    List<T> selectByMap(@Param("cm") Map<String, Object> var1);

    T selectOne(Wrapper<T> queryWrapper);

    Integer selectCount(@Param("ew") Wrapper<T> var1);

    List<T> selectList(@Param("ew") Wrapper<T> var1);

    List<Map<String, Object>> selectMaps(@Param("ew") Wrapper<T> var1);

    List<Object> selectObjs(@Param("ew") Wrapper<T> var1);

    IPage<T> selectPage(Page var1, Wrapper<T> var2);

}
