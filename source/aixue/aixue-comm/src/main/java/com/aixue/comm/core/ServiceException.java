package com.aixue.comm.core;

public class ServiceException extends BaseException {

    public ServiceException(String message, Integer code) {
        super(message,  code);
    }

    public ServiceException(String message) {
        super(message,  500);
    }

}
