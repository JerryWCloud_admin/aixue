package com.aixue.comm.pojo;

import com.aixue.comm.base.SqlCondition;
import lombok.Data;

import java.util.List;

@Data
public class QueryLIstParam {

    private Integer pageSize;

    private Integer pageNum;

    private List<SqlCondition> conditions;

}
