package com.aixue.comm.bean;

import java.util.Optional;

import com.aixue.comm.base.ValidateType;
import com.aixue.utils.pojo.Result;

public class BaseBean {

    public Optional<Result> validate(){
        return Optional.empty();
    }

    public Optional<Result> validate(ValidateType method){
        switch (method){
            case QUERY: return vQuery();
            case DETELE: return vDelete();
            case INSERT: return vInsert();
            case UPDATE: return vUpdate();
            case QUERYLIST: return vQueryList();
        }
        return Optional.empty();
    }

    protected Optional<Result> vInsert(){
       return Optional.empty();
    }

    protected Optional<Result> vDelete(){
       return Optional.empty();
    }

    protected Optional<Result> vUpdate(){
       return Optional.empty();
    }

    protected Optional<Result> vQuery(){
       return Optional.empty();
    }

    protected Optional<Result> vQueryList(){
       return Optional.empty();
    }
}
