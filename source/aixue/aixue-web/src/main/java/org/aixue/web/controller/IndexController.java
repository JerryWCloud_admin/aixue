package org.aixue.web.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RefreshScope
public class IndexController {

    @Value("${aixue.server.username}")
    private String username;
    
    @RequestMapping("/")
    public Map<String,Object> index(){
        log.info("welcome to my space,it is only to test!");
        
        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("case1", "welcome to my space,it is only to test!");
        maps.put("case2", username);
        return maps;
    }
}
