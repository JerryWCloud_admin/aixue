package org.aixue.member.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/index")
public class IndexController {

	@Value("${server.port:}")
	private Integer port;

	/**   
	 * @Title: welcome   
	 * @Description:    
	 * @param: @return      
	 * @return: String      
	 * @throws   
	 */
	@GetMapping("/welcome")
	public String welcome() {
		log.debug("welcome method!");
		return "if you see this it is means successful. port:" + port;
	}

}
