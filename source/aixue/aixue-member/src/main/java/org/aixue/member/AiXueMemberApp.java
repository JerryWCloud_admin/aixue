package org.aixue.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * 
 * @author Administrator
 *
 */
@SpringBootApplication
@EnableEurekaClient
@EnableRedisHttpSession
public class AiXueMemberApp {
	
	public static void main(String[] args) {
		SpringApplication.run(AiXueMemberApp.class, args);
	}
	
}
