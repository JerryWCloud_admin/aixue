package org.aixue.member.mapper;

import com.aixue.comm.bean.member.Member;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface MemberMapper extends BaseMapper<Member> {
}
