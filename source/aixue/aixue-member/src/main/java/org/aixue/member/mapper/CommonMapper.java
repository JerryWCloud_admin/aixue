package org.aixue.member.mapper;

import com.aixue.comm.bean.member.Common;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface CommonMapper extends BaseMapper<Common> {
}
