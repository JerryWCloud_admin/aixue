package org.aixue.member.mapper;

import com.aixue.comm.bean.member.Admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AdminMapper extends BaseMapper<Admin> {
}
