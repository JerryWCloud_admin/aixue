package org.aixue.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**   
 * @ClassName:  ConfigApp   
 * @Description:分布式配置中心  
 * @author: JerryW 
 * @date:   2019年1月16日 下午1:37:05   
 *     
 * @Copyright: 2019 www.aixue.com Inc. All rights reserved. 
 * 
 */
@EnableConfigServer
@SpringBootApplication
@EnableDiscoveryClient
public class ConfigApp {
    
    public static void main( String[] args ){
        SpringApplication.run(ConfigApp.class, args);
    }
}
