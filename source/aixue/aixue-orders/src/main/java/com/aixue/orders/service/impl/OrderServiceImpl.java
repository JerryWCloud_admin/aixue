package com.aixue.orders.service.impl;

import com.aixue.comm.base.BaseServiceImpl;
import com.aixue.comm.bean.goods.Goods;
import com.aixue.comm.bean.orders.Orders;
import com.aixue.comm.core.ServiceException;
import com.aixue.orders.clients.GoodsClient;
import com.aixue.orders.mapper.OrdersMapper;
import com.aixue.orders.service.OrderService;
import com.aixue.utils.pojo.Result;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Service
public class OrderServiceImpl extends BaseServiceImpl<Orders> implements OrderService {

    @Autowired
    private OrdersMapper ordersMapper;

    @Resource
    private GoodsClient goodsClient;

    @GlobalTransactional
    @Transactional
    @Override
    public Orders order(String goodsId) {
        log.info("开始商品下单流程,{}", RootContext.getXID());

        log.debug("开始更新商品信息");

        Result result = goodsClient.byGoods(goodsId);

        if(!result.isResult()){
            throw new ServiceException("更新商品信息失败",500);
        }
        Goods goods = new Goods();
        BeanMap.create(goods).putAll((Map) result.getData());
        log.debug("获取下单的商品信息为{}",goods);

        Orders o  = new Orders();
        o.setOrderNo(UUID.randomUUID().toString().replace("-",""));
        o.setPayTime(new Date());
        o.setAddTime(o.getPayTime());
        o.setDiscount(1);
        o.setMemberId("001");
        o.setStatus(true);
        o.setRealPay(goods.getPrices());
        o.setTotalPay(goods.getPrices());
        o.setGoodsId(goodsId);
        o.setGoodsName(goods.getGoodsName());
        log.debug("构造订单信息,{}",o);

        int insert = ordersMapper.insert(o);
        log.debug("添加订单结果为,{}", insert == 1);

        if(1 != insert){
            throw  new ServiceException("添加订单信息失败!");
        }

        return o;
    }

    @Override
    protected BaseMapper<Orders> getBaseMapper() {
        return ordersMapper;
    }
}
