package com.aixue.orders.controller;

import com.aixue.comm.base.BaseController;
import com.aixue.comm.base.BaseService;
import com.aixue.comm.bean.orders.Orders;
import com.aixue.orders.service.OrderService;
import com.aixue.utils.pojo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/orders/orders")
public class OrdersController extends BaseController<Orders> {

    @Autowired
    private OrderService orderService;

    @GetMapping("/insert/{goodsId}")
    public Result insert(@PathVariable String goodsId){
        log.debug("下单商品的id,{}",goodsId);
        return Result.success("下单成功!",orderService.order(goodsId));
    }

    @Override
    protected BaseService getBaseService() {
        return orderService;
    }
}
