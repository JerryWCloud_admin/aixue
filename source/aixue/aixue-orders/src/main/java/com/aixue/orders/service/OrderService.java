package com.aixue.orders.service;


import com.aixue.comm.base.BaseService;
import com.aixue.comm.bean.orders.Orders;

public interface OrderService extends BaseService<Orders> {

    /**
     * 下单
     * @return
     */
    Orders order(String goodsId);
}
