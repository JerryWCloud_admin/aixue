package com.aixue.orders.mapper;

import com.aixue.comm.bean.orders.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface OrdersMapper extends BaseMapper<Orders> {

}