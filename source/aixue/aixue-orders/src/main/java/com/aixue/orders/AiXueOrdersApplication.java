package com.aixue.orders;

import com.alibaba.druid.pool.DruidDataSource;
import io.seata.rm.datasource.DataSourceProxy;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**   
 * @ClassName:  AiXueApiApp   
 * @Description:
 * @author: JerryW 
 * @date:   2018年9月22日 下午6:32:06   
 *     
 * @Copyright: 2018 
 * 
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@MapperScan({"com.baomidou.mybatisplus.samples.quickstart.mapper","com.aixue.orders.mapper"})
public class AiXueOrdersApplication
{
    public static void main( String[] args )
    {
        SpringApplication.run(AiXueOrdersApplication.class, args);
    }
}
