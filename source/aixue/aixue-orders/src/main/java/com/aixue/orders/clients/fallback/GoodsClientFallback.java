package com.aixue.orders.clients.fallback;

import com.aixue.orders.clients.GoodsClient;
import com.aixue.utils.pojo.Result;

public class GoodsClientFallback implements GoodsClient {

    @Override
    public Result byGoods(String id) {
        return null;
    }
}
