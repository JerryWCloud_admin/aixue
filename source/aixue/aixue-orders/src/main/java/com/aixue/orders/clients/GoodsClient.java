package com.aixue.orders.clients;

import com.aixue.orders.clients.fallback.GoodsClientFallback;
import com.aixue.utils.pojo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "aixueGoods",fallback = GoodsClientFallback.class)
public interface GoodsClient {

    @GetMapping("/goods/goods/byGoods/{id}")
    Result byGoods(@PathVariable String id);
}
