package com.aixue.server.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import lombok.extern.slf4j.Slf4j;

import static org.springframework.security.core.userdetails.User.builder;

/**   
 * @ClassName:  WebSecurityConfig   
 * @Description:TODO  
 * @author: JerryW 
 * @date:   2018年11月3日 下午1:40:37   
 *     
 * @Copyright: 2018 
 * 
 */
@Configuration
@EnableWebSecurity
@PropertySource(value = "classpath:application.yml")
@Slf4j
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${aixue.server.username}")
    private String username;
    
    @Value("${aixue.server.password}")
    private String password;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER);
        http.csrf().disable(); //关闭CSRF攻击防御，如不关闭，Eureka Client则会找不到Eureka Server而报异常。
        //注意：为了可以使用 http://${user}:${password}@${host}:${port}/eureka/ 这种方式登录,所以必须是httpBasic,如果是form方式,不能使用url格式登录
        http.authorizeRequests().anyRequest().authenticated().and().httpBasic();
    }

    @Bean
    public UserDetailsService userDetailsService() {
       
        
        //User.UserBuilder users = User.withDefaultPasswordEncoder(); // 此方法已经不推荐使用
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        User.UserBuilder var10000 = builder();
        encoder.getClass();
        User.UserBuilder users =  var10000.passwordEncoder(encoder::encode);

        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        
        log.info(String.format("===================================>username:%s,password:%s",username,password));
        
        manager.createUser(users.username(username).password(password).roles("USER").build());
        
        return manager;
    }

    /**
     * 此方式在 Edgware.SR4 可用,在Finchley.SR1中不可用
     */
    //@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        //        auth.inMemoryAuthentication().withUser("xiaohei").password("1111").roles("USER")
        //               .and().withUser("jackma").password("1111").roles("USER", "ADMIN");
    }
}