package com.aixue.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**   
 * @ClassName:  AiXueServer   
 * @Description:TODO  
 * @author: JerryW 
 * @date:   2018年11月3日 下午1:55:45   
 *     
 * @Copyright: 2018 
 * 
 */
@EnableEurekaServer
@SpringBootApplication
public class AiXueServer 
{
    public static void main( String[] args )
    {
        SpringApplication.run(AiXueServer.class, args);
    }
}
