package com.aixue.goods;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 商品微服务
 */
@SpringBootApplication
@EnableEurekaClient
@MapperScan({"com.baomidou.mybatisplus.samples.quickstart.mapper","com.aixue.goods.mapper"})
public class GoodsApp 
{
    public static void main( String[] args )
    {
        SpringApplication.run(GoodsApp.class,args);
    }
}
