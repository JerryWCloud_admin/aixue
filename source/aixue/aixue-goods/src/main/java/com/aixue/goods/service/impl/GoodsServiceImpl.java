package com.aixue.goods.service.impl;

import com.aixue.comm.base.BaseServiceImpl;
import com.aixue.comm.bean.goods.Goods;
import com.aixue.comm.core.ServiceException;
import com.aixue.goods.mapper.GoodsMapper;
import com.aixue.goods.service.GoodsService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.seata.core.context.RootContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class GoodsServiceImpl extends BaseServiceImpl<Goods> implements GoodsService {

    @Autowired
    private GoodsMapper goodsMapper;


    @Override
    protected BaseMapper<Goods> getBaseMapper() {
        return goodsMapper;
    }

    @Transactional
    @Override
    public Goods buyGoods(String id) {
        log.info("更新商品的信息,{},{}", RootContext.getXID(),id);
        Goods goods = goodsMapper.selectById(id);
        if(null == goods){
            throw new ServiceException("货物不存在!");
        }
        goods.setCount(goods.getCount() - 1);
        if(goods.getCount() <= 0){
            throw new ServiceException("货物的库存不足");
        }
        Integer result = goodsMapper.updateById(goods);
        if(1 != result){
            throw new ServiceException("更新货物的库存失败");
        }
        return goods;
    }
}
