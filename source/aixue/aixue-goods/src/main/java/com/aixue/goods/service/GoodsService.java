package com.aixue.goods.service;

import com.aixue.comm.base.BaseService;
import com.aixue.comm.bean.goods.Goods;

public interface GoodsService extends BaseService<Goods> {

    Goods buyGoods(String id);
}
