package com.aixue.goods.controller;

import com.aixue.comm.base.BaseController;
import com.aixue.comm.base.BaseService;
import com.aixue.comm.bean.goods.Goods;
import com.aixue.goods.service.GoodsService;
import com.aixue.utils.pojo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/goods/goods")
public class GoodsController extends BaseController<Goods> {

    @Autowired
    private GoodsService goodsService;

    @Override
    protected BaseService getBaseService() {
        return this.goodsService;
    }

    /**
     * 根据商品id查询商品的信息
     * @param id 商品id
     * @return
     */
    @GetMapping("/query/{id}")
    public Result query(@PathVariable String id){
        return Result.success("success",goodsService.selectById(id));
    }

    /**
     * 根据商品idx修改商品的信息
     * @param id 商品信息
     * @return Goods
     */
    @GetMapping("/byGoods/{id}")
    public Result byGoods(@PathVariable String id){
        Goods result = goodsService.buyGoods(id);
        log.debug("更新商品结果,{}",result);
        return Result.success("success",result);
    }

}
