package com.aixue.goods.mapper;

import com.aixue.comm.bean.goods.Goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface GoodsMapper extends BaseMapper<Goods> {
}