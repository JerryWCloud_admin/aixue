# aixue

#### 项目介绍
本项目使用Sring Cloud 架构搭建项目，提供数据库分库分表。
Redis缓存数据、分布式事物、基于Spring Security安全框架实现系统信息安全。

#### 软件架构
软件架构说明
1. Spring Boot 2.0.4.RELEASE
2. Spring Cloud Finchley.SR1
3. MySQL 8.0
4. Redis 5.0
5. Mybatis 2.0+ Mybatis 3.0
6. JDk 8
7. 分布式事物TX-LCN 5.0
8. 消息队列RabbitMQ 3.7
#### 安装教程

1. 安装Mysql数据库至少5.7以上版本。
2. 安装JDK至少1.8及其以上的版本。
3. 安装Redis数据库5.0以上版本。
4. 安装Maven3.6以上版本。
5. 安装RabbitMQ运行环境Erlang及RabbitMQ服务端。
由于rabbitmq是基于erlang语言开发的，所以必须先安装erlang。安装erlang环境 

   5.1安装 gcc源码编译程序
   ``yum -y install gcc glibc-devel make ncurses-devel openssl-devel xmlto perl wget gtk2-devel binutils-devel``
   5.2下载erlang源码
   ``wget http://erlang.org/download/otp_src_22.1.tar.gz``
   5.3 解压文件包
   ``tar -zxvf otp_src_22.1.tar.gz``
   移动到/ust/local目录下 ``mv otp_src_22.1 /usr/local/``
   5.4 创建即将安装的目录
   ``mkdir ../erlang``
   5.5 配置安装路径
   ``./configure --prefix=/usr/local/erlang`` 
   5.6 安装
   `` make install``
   如果报错``Makefile:473: /usr/local/otp_src_22.1/make/x86_64-unknown-linux-gnu/otp.mk: 没有那个文件或目录``
   运行命令 ``yum install ncurses-devel.x86_64`` 然后再安装
   5.7 查看一下是否安装成功
   ``ll /usr/local/erlang/bin``
   5.8 刷新环境变量
   ``source /etc/profile``
   5.9 下载RabbitMQ 
   ``wget http://www.rabbitmq.com/releases/rabbitmq-server/v3.6.15/rabbitmq-server-generic-unix-3.6.15.tar.xz ``
   5.10 解压文件
    ``xz -d rabbitmq-server-generic-unix-3.8.0.tar.xz``
    ``tar -xvf rabbitmq-server-generic-unix-3.8.0.tar``
   5.11 配置rabbitmq环境变量：    
    vi /etc/profile
    在后面加上：export PATH=$PATH:/usr/local/rabbitmq/sbin ，这个路径是自定义的路径
   5.12 相关命令
   启动 rabbitmq-server -detached 停止 rabbitmqctl stop 状态 rabbitmqctl status
6. 安装Git代码管理工具克隆蹦仓库代码。  

#### 使用说明

1. 将项目导入idea或者eclipse工具，先运行服务注册中心。
2. 运行配置中心，配置中心默认为git仓库地址，也可以修改为本地仓库地址。
3. 运行路由服务。
4. 运行相关的微服务模块即可。

#### 长期维护 
本项目主要用于个人日常学习技术的实践，将长期维护项目。更新项目的架构和相关的技术。
